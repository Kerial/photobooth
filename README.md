# Photobooth DIY with Raspberry Pi for beginners
*Published on 08/2021*

<div align="center">
  <img src="./assets/Photobooth.jpg" alt="The Photobooth DIY" width="350"/>
</div>

## The project

I wanted to create a Photobooth and thus bought a Raspberry Pi 3B+.

I looked over the Internet and found some tutorials, but they were never for total beginners as I was. Finally, I found some base code, changed and improved it in my way and reached my goal.

The aim of this project is to explain step-by-step how to build a Photobooth with all details and pictures.

With this DIY photobooth, all is stored on Raspberry Pi and you can for now :
* take a photo
* see previous photos
* print a specific photo

## Furnitures

### Electronics

* A Raspberry Pi (I bought a 3B+, but this should work with lower, but I'm not an expert so, it's at your risk).  
Personally, I bought the starter kit [there](https://www.kubii.fr/kits-raspberry-pi-3-et-3/2121-starter-kit-officiel-pi-3-b-kubii-3272496009998.html)  
 **81,95 €**

* A Camera module.  
I bought [this one](https://www.kubii.fr/cameras-accessoires/2195-module-camera-5mp-avec-focus-ajustable-kubii-3272496011090.html)  
 **16.63€**  
 (If you want to take photo by night, you can find camera with night vision.)

* Jump wires + breadboard  
3 female - male  
2 male - male  
It's not expensive, you better buy some jump wires of different kind for future projects.  
I bought jumper wires [there](https://fr.aliexpress.com/item/1005002000655439.html?spm=a2g0o.search0304.0.0.49e33b05arFGg6&algo_pvid=53a04610-dc6f-4865-b003-8620f45261d5&aem_p4p_detail=202108091114582686633518906850002505338&algo_exp_id=53a04610-dc6f-4865-b003-8620f45261d5-6)  
My advice: go over AliExpress and find a kit with breadboard + jump wires + some stuff for future projects, like [this one](https://fr.aliexpress.com/item/4000689310993.html?spm=a2g0o.productlist.0.0.ca632a6bqAEnYq&algo_pvid=16d2f46f-92c8-4f93-8fd0-ad886933e92b&algo_exp_id=16d2f46f-92c8-4f93-8fd0-ad886933e92b-6)  
  **5€** and you have other stuff

* A remote control  
At the beginning, I bought [this](https://fr.aliexpress.com/item/32663991536.html?spm=a2g0o.productlist.0.0.4c8412bctbMkuQ&algo_pvid=937d57d4-1595-45c7-a0f3-b3b09d74c309&algo_exp_id=937d57d4-1595-45c7-a0f3-b3b09d74c309-15)  
Finally, I just use the remote control and not the receptor, so you should better find just a remote control with buttons (4 for me) in 315/433 MHz.  
Like [this one](https://fr.aliexpress.com/item/1005002879569960.html?spm=a2g0o.productlist.0.0.23902f8bZgZLvb&algo_pvid=51a39b4e-40db-4d84-88a3-7371c2d80d95&algo_exp_id=51a39b4e-40db-4d84-88a3-7371c2d80d95-0)  
 **2€**  

* The 433 MHz receptor  
 For the receptor, I didn't found what I was looking for on AliExpress, so I bought it [on Amazon](https://www.amazon.fr/WINGONEER-433Mhz-Superheterodyne-r%C3%A9cepteur-Arduino/dp/B06XHJMC82)  
 You perhaps want to change frequency or use a remote control or receptor you already have. If so, you just have to get the good pair remote control / receptor to do the job.  
  **7,99€**

### Others

* A screen and its cables (power and HDMI (or VGA/DVI + adapter)).  
Mine was of 44,7 cm (width) * 34,7 cm (height).  
Its stand is 5 cm on front and sizes 20 cm in total.  
See the picture ./assets/screen_dimensions.png for better understanding.

* A printer  
Mine is a Canon Selphy CP1300

* A wood plank  
Mine was of 120 cm (width) * 60 cm (height) * 1,7cm (thickness)  
  **12€**

### Material

* A jigsaw (easier for cutting the plank, but a regular saw could do the job)

* A cylinder saw (and a screw-gun for the rotation)  
Mine was of 22 mm of diameter

* A hammer

* Some nails

Total without Raspberry Pi : **44€**  
Total with Raspberry Pi : **126€**  

## Realisation

### Wood

#### List of pieces

As you can see on these pictures :

<div align="center">
  <img src="./assets/wood_pieces_1.png" alt="Wood pieces" width="500"/>
  <img src="./assets/wood_pieces_2.png" alt="Wood pieces" width="500"/>
  <img src="./assets/wood_pieces_3.png" alt="Wood pieces" width="500"/>
  <img src="./assets/wood_pieces_4.png" alt="Wood pieces" width="500"/>
</div>

* 1 : over
* 2 : top right
* 3 : top left
* 4 : top front flat
* 5 : bottom front
* 6 : bottom right
* 7 : bottom left
* 8 : under (the only one not visible on the pictures)
* 9 : back
* 10 : top front

### Mathematics

#### Formulas

screen : { x<sub>s</sub> ; z<sub>s</sub> }  
wood_thickness : t  
P, L, H, F and A : see on picture ./assets/wood_pieces_2.png

{ x<sub>1</sub> ; y<sub>1</sub> } = { x<sub>s</sub> + 2 * t ; P }  
{ y<sub>2</sub> ; z<sub>2</sub> } = { P ; z<sub>s</sub> + t }  
{ y<sub>3</sub> ; z<sub>3</sub> } = { P ; z<sub>s</sub> + t }  
{ x<sub>4</sub> ; y<sub>4</sub> } = { x<sub>s</sub> ; L + F }  
{ x<sub>5</sub> ; z<sub>5</sub> } = { x<sub>s</sub> ; H }  
{ y<sub>6</sub> ; z<sub>6</sub> } = { P + L ; H + t }  
{ y<sub>7</sub> ; z<sub>7</sub> } = { P + L ; H + t }  
{ x<sub>8</sub> ; y<sub>8</sub> } = { x<sub>s</sub> + 2 * t ; P + L }  
{ x<sub>9</sub> ; y<sub>9</sub> } = { x<sub>s</sub> ; H + t + z<sub>s</sub> }  
{ x<sub>10</sub> ; y<sub>10</sub> } = { x<sub>s</sub> ; A }  

#### Measures (in cm)

{ x<sub>s</sub> ; z<sub>s</sub> } = { 44.7 ; 34.7 }  
t = 1.7  
P = 20  
L = 5  
H = 5  
F = 3  
A = 5.5  

{ x<sub>1</sub> ; y<sub>1</sub> } = { 48.1 ; 20 }  
{ y<sub>2</sub> ; z<sub>2</sub> } = { 20 ; 36.4 }  
{ y<sub>3</sub> ; z<sub>3</sub> } = { 20 ; 36.4 }  
{ x<sub>4</sub> ; y<sub>4</sub> } = { 44.7 ; 8 }  
{ x<sub>5</sub> ; z<sub>5</sub> } = { 44.7 ; 5 }  
{ y<sub>6</sub> ; z<sub>6</sub> } = { 25 ; 6.7 }  
{ y<sub>7</sub> ; z<sub>7</sub> } = { 25 ; 6.7 }  
{ x<sub>8</sub> ; y<sub>8</sub> } = { 48.1 ; 25 }  
{ x<sub>9</sub> ; y<sub>9</sub> } = { 44.7 ; 41.4 }  
{ x<sub>10</sub> ; y<sub>10</sub> } = { 44.7 ; 5.6 }  

### Practical part

#### Cutting

You can find an optimized draw of the pieces on the 120 * 60 wood plank here :

<div align="center">
  <img src="./assets/wood_plank_cutting.png" alt="The optimized cutting" width="800"/>
</div>

To get this optimized measures, I used the Android App CutList Optimizer.  

Use a jigsaw to cut the pieces, it will be easier.

Use the cylinder saw of 22 mm to cut the hole in the center of piece 10 for the camera.

#### Bindings

I put nails to group :
* 1 + 2 + 3 + 9 + 10
* 4 + 5 + 6 + 7 + 8

I finally have two parts that can be put together without nails in order to easy access to raspberry Pi and screen : I used one of the loss of the wood plank (the one under 8) to create a support for the Raspberry Pi as you can see here :

<div align="center">
  <img src="./assets/Raspberry_support_inside.jpg" alt="The Raspberry support" width="800"/>
</div>

For this piece, it depends on your screen so you should first finish all the wood and electronic parts, and at the end, see how you can put it. :)

#### Others

Finally, you just have to wire together the screen and the Raspberry Pi and put the Raspberry on its support inside the Photobooth.  
The power wires of screen and Raspberry Pi can go through the space on back of Photobooth.  
I think you're ready for code !

### Electronic circuit

Wire the camera into the Raspberry Pi.  
Put the receptor on the breadboard.  
For the bindings, I followed [this tutorial](https://www.pofilo.fr/post/20190529-home-assistant-433mhz/).  

<div align="center">
  <img src="./assets/433MHz_connection.png" alt="The electronic schema" width="350"/>
</div>

* <span style="color:black">Black</span> wire on minus (breadboard) and port 06 - GND
* <span style="color:red">Red</span> wire on plus (breadboard) and port 17 - 3.3V PWR
* <span style="color:green">Green</span> wire for data (receptor) and port 13 - GPIO27

At the end, you should have something like this (my wires are not the same color as on schema, but the position is good):

<div align="center">
  <img src="./assets/electronic_circuit_2.jpg" alt="The electronic circuit" width="350"/>
  <img src="./assets/electronic_circuit_1.jpg" alt="The electronic circuit" width="350"/>
</div>

As you can see, I only used one side of the receptor, so there might be cheaper receptor available on the Internet...

### Code

You can find the code in ./src  
Enable ssh and camera on Raspberry Pi and connect to it via ssh.  
Copy code and install dependencies.

#### Dependencies

* PiCamera for the camera module : https://picamera.readthedocs.io/en/release-1.13/

* Rpi_rf for the remote control / receptor binding : https://pypi.org/project/rpi-rf/

#### First run

Well, all should be working now, so let's run it !  
You just have to launch this command :
```console
python3 main.py
```
At this time, you should see the video from camera on your screen.  
In file `photobooth.py`, function `clicOnButton(self)`, I added a comment line. If you uncomment it, you see all the signals received by the receptor. When you push one of the buttons of the remote control, you see a specific code (`rfdevice.rx_code`) in the log of the console. This is the code of the button.  
Just put these codes in function `getButtonClicked(self)` of `photobooth.py`.

Finally, create a `photos` directory where you have the source code. The photos will be stored there.  
You're now ready to go ! :D

#### Add of the printer

Once all done, you can now plug your printer in (via USB for me).  
Configure the printer via the printer configuration menu.  
With my printer, I was unable to configure it properly, so here is how I did this (following [this tutorial](https://wikifab.org/wiki/KALO%27_MATON_Photomaton_automatique_%C3%A0_base_de_Raspberry_Pi/fr#Step_5_-_Installez_votre_imprimante)):  
*  On a terminal, type : `sudo usermod -a -G lpadmin pi` and then `sudo /etc/init.d/cups restart` to restart the CUPS service.  
*  Open Chromium and go on localhost:631 --> "Adding printers and Classes" and add your printer (connect yourself with your user/pwd, default to pi/raspberry).  
If your printer is not listed, go [here](http://www.openprinting.org/printers) and download your CUPS driver.
*  On a terminal, type : `sudo vim /etc/cups/cupsd.conf` (or open it with nano if you prefer) and add `Allow @local` on the following lines :  
```html
# Restrict access to the server...
<Location>
      Order allow,deny
      Allow @local
</Location>
# Restrict access to the admin pages...
<Location /admin>
      Order allow,deny
      Allow @local
</Location>
# Restrict access to configuration files...
<Location /admin/conf>
      AuthType Default
      Require user @SYSTEM
      Order allow,deny
      Allow @local
# Restrict access to log files...
</Location /admin/log>
      Order allow,deny
      Allow @local
</Location>
```

To test if your printer is correctly installed, you can try to print a document with the following command :  
`lp -d yourPrinterName yourDocumentToPrint`

For example, I named my printer "myPrinter" and the document "image.jpg" :  
`lp -d myPrinter image.jpg`  

Finally, you have to modify the line 134 of `photobooth.py` by changing "nomImprimante" with your printer name, and "/home/pi/Photobooth/photos/" with the directory where you store your photos.

#### Run program at startup

I followed [this tutorial](https://www.instructables.com/Raspberry-Pi-Launch-Python-script-on-startup/).

#### Functionalities

* When you're in photo mode (you see the video of the camera) :
  * Button A : takes a picture, displays this picture for 3 seconds and goes back to photo mode
  * Button B : displays last taken picture (goes into picture mode)

* When you're in picture mode (you see taken pictures) :
  * Button A : ask to print the displayed picture
  * Button B : displays previous picture
  * Button C : displays next picture
  * Button D : returns to photo mode

* When you're in confirmation mode (after asking to print the photo) :
  * Button A : print the photo and go back to photo mode
  * Button D : cancel the print and go back to photo mode

## Conclusion

It was very interesting to do this project and this tutorial.  
If you have any comment / question, feel free to create a PR, I'll try to answer it the best I can ! :)
