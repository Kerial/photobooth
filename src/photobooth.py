#!/usr/bin/env python3

import argparse
import signal
import sys
import logging
import os

from rpi_rf import RFDevice
from picamera import PiCamera
import time
import cv2
import subprocess

from mode import Mode
from button import Button

class Photobooth:
    def __init__(self):
        logging.basicConfig(level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S',
                format='%(asctime)-15s - [%(levelname)s] %(module)s: %(message)s', )

        parser = argparse.ArgumentParser(description='Receives a decimal code via a 433/315MHz GPIO device')
        parser.add_argument('-g', dest='gpio', type=int, default=27,
                    help="GPIO pin (Default: 27)")
        args = parser.parse_args()

        signal.signal(signal.SIGINT, self.exithandler)
        self.rfdevice = RFDevice(args.gpio)
        self.rfdevice.enable_rx()
        logging.info("Listening for codes on GPIO " + str(args.gpio))

        self.camera = PiCamera()
        os.putenv("DISPLAY", ":0.0")

        self.lastCommandTimestamp = self.rfdevice.rx_code_timestamp
        self.mode = Mode.PHOTO
        self.timestampLastPhoto = None
        self.displayedPicture = None
        self.confirmWindow = "confirm"

    # pylint: disable=unused-argument
    def exithandler(self, signal, frame):
        self.rfdevice.cleanup()
        sys.exit(0)

    def captureImageAndDisplay(self):
        pictureToDisplay = self.captureImage()
        self.exitPhotoMode()
        self.displayPictureAndWait(pictureToDisplay)
        self.goToPhotoMode()

    def captureImage(self):
        pictureName = 'picture' + str(int(time.time())) + '.jpg'
        path = './photos/' + str(pictureName)
        self.camera.capture(path)
        logging.info("Photo captured !")
        return pictureName

    def displayPictureAndWait(self, pictureToDisplay):
        self.displayPicture(pictureToDisplay)
        time.sleep(3)

    def enterPhotoMode(self):
        self.camera.start_preview()
        self.mode = Mode.PHOTO

    def exitPhotoMode(self):
        self.camera.stop_preview()

    def exitPictureMode(self):
        cv2.destroyAllWindows()

    def displayPreviousPicture(self):
        pictures = self.getListOfPictures()
        pictures.reverse()
        pictureToDisplay = list(filter(self.previousPicture, pictures))[0] if self.displayedPicture else pictures[0]
        self.displayPicture(pictureToDisplay)

    def getListOfPictures(self):
        return sorted(os.listdir('./photos'))

    def displayNextPicture(self):
        pictures = self.getListOfPictures()
        picturesToDisplay = list(filter(self.nextPicture, pictures))
        if picturesToDisplay:
            self.displayPicture(picturesToDisplay[0])

    def displayPicture(self, pictureToDisplay):
        path = './photos/' + str(pictureToDisplay)
        self.displayedPicture = pictureToDisplay
        img = cv2.imread(path, cv2.IMREAD_UNCHANGED)
        cv2.imshow('image', img)
        cv2.waitKey(500)

    def previousPicture(self, picture):
        return picture < self.displayedPicture

    def nextPicture(self, picture):
        return picture > self.displayedPicture

    def updateLastCommandTimestamp(self, lastCommandTimestamp):
        self.lastCommandTimestamp = lastCommandTimestamp

    def getButtonClicked(self):
        code = self.rfdevice.rx_code
        if code == 5326016:
            return Button.A
        if code == 5325872:
            return Button.B
        if code == 5325827:
            return Button.C
        if code == 5325836:
            return Button.D
        return None

    def lastPhotoMoreThanFiveSeconds(self, currentTimestamp):
        return self.timestampLastPhoto is None or currentTimestamp > self.timestampLastPhoto + 5000000

    def clicOnButtonA(self, currentTimestamp):
        if self.mode == Mode.PHOTO and self.lastPhotoMoreThanFiveSeconds(currentTimestamp):
            self.timestampLastPhoto = currentTimestamp
            self.captureImageAndDisplay()
        elif self.mode == Mode.PICTURE:
            confirmImage = './confirm_dialog.png'
            confirm = cv2.imread(confirmImage, cv2.IMREAD_UNCHANGED)
            cv2.namedWindow(self.confirmWindow, cv2.WINDOW_NORMAL)
            cv2.moveWindow(self.confirmWindow, 600, 500)
            cv2.resizeWindow(self.confirmWindow, 500, 234)
            cv2.imshow(self.confirmWindow, confirm)
            cv2.waitKey(500)
            self.goToConfirmMode()
        elif self.mode == Mode.CONFIRM:
            lpCommand = ["lp", "-d", "nomImprimante", "/home/pi/Photobooth/photos/" + str(self.displayedPicture)]
            subprocess.Popen(lpCommand, stdout=subprocess.PIPE)
            self.exitConfirmMode()

    def clicOnButtonB(self):
        if self.mode == Mode.PHOTO:
            self.exitPhotoMode()
            self.mode = Mode.PICTURE
        if self.mode == Mode.PICTURE:
            self.displayPreviousPicture()

    def clicOnButtonC(self):
        if self.mode == Mode.PICTURE and self.displayedPicture is not None:
            self.displayNextPicture()

    def clicOnButtonD(self):
        if self.mode == Mode.PICTURE:
            self.goToPhotoMode()
            self.displayedPicture = None
        if self.mode == Mode.CONFIRM:
            self.exitConfirmMode()

    def clicOnButton(self):
        currentTimestamp = self.rfdevice.rx_code_timestamp
        buttonClicked = self.getButtonClicked()
        if buttonClicked:
            logging.info(str(self.rfdevice.rx_code) + " [pulselength " + str(self.rfdevice.rx_pulselength) + ", protocol " + str(self.rfdevice.rx_proto) + "]")
            self.updateLastCommandTimestamp(currentTimestamp)
            self.clicRealOnButton(buttonClicked, currentTimestamp)

    def clicRealOnButton(self, buttonClicked, currentTimestamp):
        if buttonClicked == Button.A: 
            self.clicOnButtonA(currentTimestamp)

        if buttonClicked == Button.B:
            self.clicOnButtonB()

        if buttonClicked == Button.C:
            self.clicOnButtonC()

        if buttonClicked == Button.D:
            self.clicOnButtonD()

    def goToPhotoModeIfNeeded(self):
        if self.mode == Mode.PICTURE and self.rfdevice.rx_code_timestamp > self.lastCommandTimestamp + 30000000:
            self.goToPhotoMode()

    def goToPhotoMode(self):
        self.exitPictureMode()
        self.displayedPicture = None
        self.enterPhotoMode()

    def goToConfirmMode(self):
        self.mode = Mode.CONFIRM

    def exitConfirmMode(self):
        cv2.destroyWindow(self.confirmWindow)
        cv2.waitKey(1)
        self.mode = Mode.PICTURE

