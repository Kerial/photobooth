from enum import Enum

class Mode(Enum):
    PHOTO = 1
    PICTURE = 2
    CONFIRMATION = 3
