#!/usr/bin/env python3

import logging
import time
from photobooth import Photobooth


def main():
    photobooth = Photobooth()
    rfdevice = photobooth.rfdevice
    photobooth.enterPhotoMode()
    while True:
        currentTimestamp = rfdevice.rx_code_timestamp
        if photobooth.lastCommandTimestamp is None or currentTimestamp > int(photobooth.lastCommandTimestamp) + 300000:
            photobooth.clicOnButton()
        time.sleep(0.01)
        photobooth.goToPhotoModeIfNeeded()

    # rfdevice.cleanup()

if __name__ == "__main__":
    main()
